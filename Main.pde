private final int MAX_BALL_COUNT = 100;
private final int POSITION_SAVES_MODULO = 3;
private final float MIN_SPEED = 2;
private final float MAX_SPEED = 6;

private ArrayList<Ball> m_balls;
private PVector m_gravity;
private int m_edgeCollisionMargin;
private int m_frameCount;

enum EdgeType
{
  Vertical,
  Horizontal
}

public void setup()
{
  size(720, 480);
  
  m_balls = new ArrayList<Ball>();
  CreateBall(new PVector(width / 2, height / 2), 25);
  
  m_gravity  = new PVector(0, 0);
  m_edgeCollisionMargin = 3;
  m_frameCount = 0;
}

public void draw()
{
  //Draw background
  fill(200, 200, 200);
  background(5);
  
  for(int i = m_balls.size() - 1; i >= 0; i--)
  {
    Ball ball = m_balls.get(i);
    color ballColor = ball.getColor();
    
    //Draw trail   
    PVector[] previousDrawPositions = ball.getStoredPositions();    
    for(int p = 0; p < previousDrawPositions.length; p++)
    {
        int alpha = 175 - 175 / previousDrawPositions.length * p;
        float radius = ball.getRadius() - ball.getRadius() / previousDrawPositions.length * p;
        
        fill(ballColor,  alpha);    
        stroke(ballColor, alpha);
        ellipse(previousDrawPositions[p].x, previousDrawPositions[p].y, radius, radius);
    }
    
    //Draw ball
    fill(ballColor, 255);
    ellipse(ball.getPosition().x, ball.getPosition().y, ball.getRadius(), ball.getRadius());
    
    //Check for collision with bounds
    if(isBallOnEdge(ball))
    {
      onEdgeCollision(ball, getCurrentCollidingEdgeType(ball));
    }  
    moveBall(ball);
  } 
  
  m_frameCount++;
}

private Ball CreateBall(PVector position, float radius)
{
  Ball ball = new Ball(position.x, position.y, radius);
  
  int xDir = 1;
  int yDir = 1;
  
  if(random(2) == 1)
    xDir = -1;
  if(random(2) == 1)
    yDir = -1;
  
  ball.setVelocity((int)random(MIN_SPEED, MAX_SPEED) * xDir, (int)random(MIN_SPEED, MAX_SPEED) * yDir);
  m_balls.add(ball);
  
  return ball;
}

private void moveBall(Ball ball)
{
  ball.translate(m_gravity);
  ball.translate(ball.getVelocity());
  
  if(m_frameCount % POSITION_SAVES_MODULO == 0)
    ball.storePosition();
}

private boolean isBallOnEdge(Ball ball)
{
  return (
  ball.getPosition().x < m_edgeCollisionMargin ||
  ball.getPosition().y < m_edgeCollisionMargin || 
  ball.getPosition().x > width - m_edgeCollisionMargin || 
  ball.getPosition().y > height - m_edgeCollisionMargin);
}

private EdgeType getCurrentCollidingEdgeType(Ball ball)
{
  if(ball.getPosition().x < m_edgeCollisionMargin || ball.getPosition().x > width - m_edgeCollisionMargin)
  {
   return EdgeType.Vertical; 
  }
  return EdgeType.Horizontal;
}

private void onEdgeCollision(Ball ball, EdgeType collisionType)
{
  PVector velocity = ball.getVelocity();
  switch(collisionType)
  {
   case Vertical:
      ball.setVelocity((int)velocity.x * -1, (int)velocity.y);
      ball.translate((int)velocity.x, 0);
   break;
   case Horizontal:
      ball.setVelocity((int)velocity.x, (int)velocity.y * -1);
      ball.translate(0, (int)velocity.y);
   break;
  }
  int ballRadius = (int)(ball.getRadius() * 0.85);
  
  if(m_balls.size() < MAX_BALL_COUNT && ballRadius >= 15)
  {
    PVector vel = CreateBall(ball.getPosition(), ballRadius).getVelocity();
    vel.x *= 0.85;
    vel.y *= 0.85;
  }
}