public class Ball
{
  private final int SAVED_POSITIONS_COUNT = 10;
  
    private PVector m_position;
    private PVector m_velocity;
    private color m_color;
    private float m_radius;
    private PVector[] m_previousPositions;
    
    public Ball(float x, float y, float radius)
    {
        m_position = new PVector(x, y);
        m_velocity = new PVector(0, 0);
        m_color = color(random(255), random(255), random(255));
        m_radius = radius;
        m_previousPositions = new PVector[SAVED_POSITIONS_COUNT];
        
        for(int i = 0; i < m_previousPositions.length; i++)
        {
          m_previousPositions[i] = new PVector(m_position.x, m_position.y);
        }
    }
    
    public void storePosition()
    {      
      for(int i =  m_previousPositions.length - 1; i >= 1; i--)
      {
         m_previousPositions[i].set(m_previousPositions[i - 1]); 
      }
      
      m_previousPositions[0].set(m_position);
    }
    
    public color getColor()
    {
        return m_color; 
    }
    
    public PVector[] getStoredPositions()
    {
       return m_previousPositions;
    }
    
    public float getRadius()
    {
       return m_radius; 
    }
    
    public PVector getPosition()
    {
       return m_position; 
    }
    
    public void translate(int x, int y)
    {
        m_position.add(x, y);
    }
    
    public void translate(PVector translation)
    {
       m_position.add(translation); 
    }
    
    public PVector getVelocity()
    {
        return m_velocity;
    }
    
    public void setVelocity(float x, float y)
    {
        m_velocity.set(x, y);
    }
}